# AprosUpdateHW

Este pequeñisimo proyecto se crea para ayudar a la actualizacion de Apros a la version 2.8.50_ACS con la _opcion_ de trabajar junto con servidor ACS version 1.2.8.Solo es posible realizar esto ultimo si la **version de Ubuntu es 16.04**. Si la version es 14.04 es posible que falle.

# Herramientas y datos necesarios:
    -Terminal: Util para la conexion SSH al servidor
    -Usuario con privilegios de root
    -El servidor con salida a internet y resolucion de DNS: Para clonar este repo e instalar docker (si es necesario)
    -MySQLWorkbench: Para sincronizar scheme de base de datos. 
    
# Steps:

1) Antes de comenzar con la actualizacion debemos crear un backup de las bases de datos.Para ello se debe ejecutar la siguente linea (copiar y pegar en el server):
`mkdir -p /root/dumps/ && mysqldump -p --all-databases > /root/dumps/dump_$(date +%F_%H-%M-%S).sql 2>/dev/null`

2) Ahora sincronizar el scheme de las bases de datos con el programa _MySQLWorkbench_, para esto se puede usar un cliente que ya tenga la ultima version, por ej. Rio Tercero

IMPORTANTE que la seleccion de **SOURCE** sea **RIO TERCERO** y en TARGET sea el cliente a actualizar

3) Ejecutar el script updateAprosHW.sh y continuar con sus indicaciones.

# Steps equipos Huawei sobre Furukawa (excluir esto si usaran equipos Huawei sobre Huawei):

1) En caso que el cliente tenga equipos Huawei sobre Furukawa, y quiera realizar la gestion desde Apros se debera contar con un server ACS, para ello se usa docker, ejecutar:
    -`apt-get update && apt-get install docker docker.io docker-compose -y`

2) Ahora ejecutar los contenedores para el funcionamiento del server ACS :
    `docker-compose up -d`

3) Si todo va bien se puede comprobar con el siguiente comando`docker ps`.Se debe ver algo asi:

```
CONTAINER ID        IMAGE                        COMMAND                  CREATED             STATUS              PORTS                                                                                            NAMES
d1503b981f9b        emilioairut/genieacs:1.2.8   "docker-entrypoint.s…"   2 weeks ago         Up 10 days          0.0.0.0:3000->3000/tcp, 0.0.0.0:7547->7547/tcp, 0.0.0.0:7557->7557/tcp, 0.0.0.0:7567->7567/tcp   genieacs
a54885cbed30        mongo:4.0                    "docker-entrypoint.s…"   2 weeks ago         Up 10 days          27017/tcp                                                                                        mongo
```

Fin de actualizacion. Be happy :)

