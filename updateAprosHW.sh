#!/bin/bash

clear
echo "Comenzando actualizacion ..."
if [[ -f /home/tecnored/deploy.sh ]];then
    echo -e "\t[ 1 ] Copiando WAR 2.8.50_ACS en /home/tecnored/"
    cp APROS-V2.8.50_ACS.war /home/tecnored/ 2>/dev/null
elif [[ -f /home/admin/deploy.sh ]];then
    echo -e "\t[ 1 ] Copiando WAR 2.8.50_ACS en /home/admin/"
    cp APROS-V2.8.50_ACS.war /home/admin/ 2>/dev/null
else
    echo -e "\t[-] >>>> No existe ni /home/tecnored/ no /home/admin/ <<<< "
    exit 1
fi


echo -e "\t[ 2 ] Deployando WAR 2.8.50_ACS ..."
if [[ -f /home/tecnored/APROS-V2.8.50_ACS.war ]];then
    /home/tecnored/deploy.sh 2.8.50_ACS >/dev/null
elif [[ -f /home/admin/APROS-V2.8.50_ACS.war ]];then
    /home/admin/deploy.sh 2.8.50_ACS >/dev/null
else
    echo -e "\t[-] >>>> Error de deploy 2.8.50_ACS <<<< "
    exit 1
fi 


echo -e "\t[ 3 ] Creando los templates de huawei en /home/furukawa/"
tar -xf carpetas_huawei.tgz -C /home/furukawa/
if [[ ! -d /home/furukawa/EG8141A5 ]];then
    echo -t "\t[-] >>>> Error en descomprimir archivo con los templates de huawei <<<< "
    exit 1
else
    echo -e "\t\tIMPORTANTE!!"
    echo -e "\t\tA) Dentro de cada template agregar y/o modificar la IP dentro de la ACL para conectar remotamente.Debe ser el rango de ip privada del Apros y la ip publica de Tecnored"
    echo -e "\t\tB) Tambien deben cambiar la contraseña de support"
fi


ip_apros=$(mysql -uroot -pFrom\$hell#2003 -e "select value_parameter from aprosftthdata.parameter where name_parameter = 'ftp_ip';" 2>&1 | egrep -v '(Warning|value_parameter)')
echo -e "\t[ 4 ] Insertando valores en base de datos..."
cat > /tmp/datos_temporal.txt << EOF
        #A) OLT MARK
            INSERT IGNORE INTO aprosftthdata.olt_mark (id_olt_mark, name) VALUES ('8', 'Huawei');

        #B) OLT MODEL
			INSERT IGNORE INTO aprosftthdata.olt_model (id_olt_model, id_olt_mark, name, boards_quantity, ports_by_board) VALUES 
				('8', '8', 'MA5801', '1', '8'),
				('9', '8', 'MA5800X2', '2', '16'),
				('10', '8', 'MA5800X7', '7', '16');

		#C) OLT APROVAL
			INSERT IGNORE INTO aprosftthdata.olt_approval (id_olt_approval, id_olt_model, version_firmware_olt, action_olt_aproval) VALUES 
				('12', '8', 'MA5801V100R019C00', '4'),
				('13', '9', 'MA5800V100R019C12', '4'),
				('14', '10', 'MA5800V100R019C12', '4');

		#D) ONU PROFILE TYPE (Comprobar en la DB de laboratorio si existen mas modelos y agregarlos.)
		    INSERT IGNORE INTO aprosftthdata.onu_profile_type (id_onu_profile_type, type) VALUES 
		        ('9', 'HG8247H5'),
		        ('10', 'HG8245H5'),
		        ('11', 'EG8141A5'),
		        ('12', 'HG8010H'),
		        ('13', 'EG8145V5');

		#F) ONU_TYPE (Comprobar en la DB de laboratorio si existen mas modelos y agregarlos.)
		    INSERT IGNORE INTO aprosftthdata.onu_type (id_onu_type, name, image, version, xml_type, id_onu_profile_type, password_wifi, password_admin_web, management_port, ip_dns, firewall, ssid, mac_address_prefix) VALUES
		        ('13', 'HG8247H5', "", '1.0', 'HG8247H5', '9', '12345678', '12345678', '6415', '186.148.233.10,8.8.8.8', '0', 'FKW_USERSURNAME', ''),
		        ('14', 'HG8245H5', "", '1.0', 'HG8245H5', '10', '12345678', '12345678', '6415', '186.148.233.10,8.8.8.8', '0', 'FKW_USERSURNAME', ''),
		        ('15', 'EG8141A5', "", '1.0', 'EG8141A5', '11', '12345678', '12345678', '6415', '186.148.233.10,8.8.8.8', '0', 'FKW_USERSURNAME', ''),
		        ('16', 'HG8010H', "", '1.0', 'HG8010H', '12', '12345678', '12345678', '6415', '186.148.233.10,8.8.8.8', '0', 'FKW_USERSURNAME', ''),
		        ('17', 'EG8145V5', "", '1.0', 'EG8145V5', '13', '12345678', '12345678', '6415', '186.148.233.10,8.8.8.8', '0', 'FKW_USERSURNAME', '');
		
        #G) ONU APPROVAL (Comprobar en la DB de laboratorio si existen mas modelos y agregarlos.)
		        INSERT IGNORE INTO aprosftthdata.onu_approval (id_onu_approval, id_onu_type, version_firmware_onu, action_onu_aproval) VALUES
		        ('13', '13', '1.0', '1'),
		        ('14', '14', '1.0', '1'),
		        ('15', '15', '1.0', '1'),
		        ('16', '16', '1.0', '1'),
		        ('17', '17', '1.0', '1');

		#H) TRAFFIC TABLE
			INSERT IGNORE INTO aprosftthdata.traffic_table (id, up, down, name, index_col) VALUES 
				('35', '2048', '2048', 'test', '0'),
				('36', '102400', '102400', 'test', '0'),
				('37', '10240', '10240', 'test', '0'),
				('38', '1024', '1024', 'test', '0'),
				('39', '1012736', '1012736', 'test', '0');

        #6) ACTUALIZAR LAS VLANS
        		update aprosftthdata.vlan set id_olt='0';

        #7) REVISAR QUE DUAL_STACK esta en 0
        		update aprosftthdata.reservation_onu set dual_stack='0';

        # IP SERVER ACS
        	INSERT IGNORE INTO aprosftthdata.parameter (id_parameter, name_parameter, value_parameter, description, visible) VALUES ('36', 'ipServerACS', '$ip_apros:7557', '', '0');        

EOF
mysql -uroot -pFrom\$hell#2003 < /tmp/datos_temporal.txt 2>&1 | grep -v Warning
echo -e "\t[ 5 ] Fin de automatizacion :)"


cp /home/furukawa/params.xml{,_$(date +%F_%s)}
echo -e "\n\n"
echo "Siguientes pasos a realizar manualmente:"
echo -e "
En el server por ssh:
    Editar modelo 'params.xml' que se encuentra aqui ( dentro de la carpeta aprosupdatehw )
    Modificarlo con los parametros del cliente que tiene actualmente en /home/furukawa/params.xml
    Una vez modificado hacer esto: cp params.xml /home/furukawa/params.xml
	
	Nota:
	Por precaucion se genera automaticamente un backup del /home/furukawa/params.xml al ejecutar el script 		updateAprosHW.sh y se puede encontrar en /home/furukawa/ 
	Ejemplo de archivo backup: /home/furukawa/params.xml_2022-07-20_1658352018
"

echo "
Si el cliente ya tiene una OLT HUAWEI se debe hacer lo siguiente desde APROS WEB:
	A) Cargar la OLT HUAWEI
	B) Cargar VLANS de la OLT HUAWEI
	C) Cargar planes
"
echo ""
echo "Fin de actualizacion. ¡Felicidades!"
echo ""

exit 0
